package com.homework.veter.simplefileexplore;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;



public class FileExploreActivity extends ListActivity {

    private File mCurrentDir;
    private TextView showCurrentDir;
    private static final String BUNDLE_CURRENT_DIRECTORY = "current_directory";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        showCurrentDir = (TextView) findViewById(R.id.currentDir);
//        TextView emptyView = new TextView(this);
//        emptyView.setText("Nothing to show");
//        getListView().setEmptyView(emptyView);
        if (savedInstanceState != null) {
            browseTo(new File(savedInstanceState.getString(BUNDLE_CURRENT_DIRECTORY)));
        } else {
            mCurrentDir = Environment.getExternalStorageDirectory();
            String state = Environment.getExternalStorageState();
            if (Environment.MEDIA_MOUNTED.equals(state)) {
                fill(mCurrentDir);
            } else {
                AlertDialog.Builder builder = new AlertDialog.Builder(getApplication());
                builder.setTitle("Attention")
                        .setMessage("Accses Eror!")
                        .setCancelable(false)
                        .setNegativeButton("ОК",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        finish();
                                    }
                                });
                AlertDialog alert = builder.create();
                alert.show();
            }
        }
    }

    private void fill(File file) {
        if (file.list().length <= 0) {
            Intent intent = new Intent(getApplicationContext(), FileNothingToShow.class);
            startActivity(intent);
        } else {
            File[] dirs = file.listFiles();
            if (dirs == null) {
                Toast.makeText(getApplicationContext(), "Eror", Toast.LENGTH_LONG).show();
                upOneLevel();
            }
            List<File> dir = Collections.unmodifiableList(Arrays.asList(dirs));
            showCurrentDir.setText("Current Dir: " + file.getName());
//        TextView headerView = new TextView(this);
//        headerView.setText("..");
//        getListView().addHeaderView(headerView);
            FileListAdapter mFileListAdapter = new FileListAdapter(FileExploreActivity.this, android.R.layout.simple_list_item_1, dir);
            setListAdapter(mFileListAdapter);
        }
    }

    @Override
    public FileListAdapter getListAdapter() {
        return (FileListAdapter) super.getListAdapter();
    }

    @Override
    protected void onListItemClick(ListView listView, View view, int position, long id) {
//        if (position == 0) {
//            upOneLevel();
//        }
        browseTo(getListAdapter().getItem(position));
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(BUNDLE_CURRENT_DIRECTORY, mCurrentDir.getAbsolutePath());

    }

    private void onFileClick() {
        Toast.makeText(this, "File Clicked in " + mCurrentDir + " folder", Toast.LENGTH_SHORT).show();
    }

    private void upOneLevel() {
        if (mCurrentDir.getParent() != null) {
            browseTo(mCurrentDir.getParentFile());
        }
    }

    @Override
    public void onBackPressed() {
        upOneLevel();
    }

    private void browseTo(File parentFile) {
        if (parentFile.isDirectory()) {
            mCurrentDir = new File(parentFile.getPath());
            fill(mCurrentDir);
        } else {
            onFileClick();
        }
    }
}
