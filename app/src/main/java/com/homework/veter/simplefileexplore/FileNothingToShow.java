package com.homework.veter.simplefileexplore;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class FileNothingToShow extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nothing);
        TextView mNothing = (TextView) findViewById(R.id.nothing);
        mNothing.setText(R.string.nothing_to_show);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(getApplicationContext(), FileExploreActivity.class);
        startActivity(intent);
        finish();
    }
}
